#include <iostream>
#include <string>

using namespace std;

int main()
{
	setlocale(LC_CTYPE, "");
	cout << "Pavlosoft Pindows [Version 0.1.17]" << endl <<
	"(c) Корпорация Павлософт (Pavlosoft Corp.), 2014. Все права защищены." << endl << endl;
	
	string cmd;
	string dir = "C:\\Pindows\\System32>";
	
	bool apache = false;
	bool phplink = false;
	bool ftpserver = false;
	int lastcode = 0;
	
	for(int i = 0; i < 1;)
	{ 
		cout << dir;
		getline(cin, cmd);
		
		if(cmd.empty())
		{
			cout << "";
		}
		
		else if(cmd == "help" || cmd == "help eggs")
		{
			if(cmd == "help")
			{
				cout << "Среда разработки Павела Лекшина:" << endl << endl <<
				
				"Веб-разработка:" << endl << endl <<
				"help - показать данную справку" << endl <<
				"exit - покинуть сессию Telnet" << endl <<
				"php - запуск PHP-FPM для Apache" << endl <<
				"apache - запуск веб-сервера Apache" << endl <<
				"ftp - запуск FTP сервера" << endl << endl <<
				
				"Разработка прикладного ПО:" << endl << endl <<
				"showcode - посмотреть последние разработки" << endl <<
				"c++ - разработать на C++" << endl <<
				"c# - сговнокодить на C#" << endl <<
				"java - писать моды для Minecraft" << endl <<
				"pascal - написать Hello World" << endl <<
				"assambler - думать над цифрами" << endl <<
				"pavlocode - написать на PHP программу для ПК" << endl;
			}
			
			else
			{
				cout << "Пасхальные яйца среды разработки:" << endl << endl <<
				"nginx - информация про Hostinger" << endl << endl;
			}
		}
		
		else if(cmd == "exit")
		{
			cout << "Закртытие сессии Telnet произведено успешно." << endl << endl;
			i++;
		}
		
		else if(cmd == "php")
		{
			if(apache == false)
			{
				cout << "[PHP] Error, start Apache service first." << endl << endl;
			}
			
			else
			{
				if(phplink == false)
				{
					phplink = true;
					cout << "[PHP] Starting PHP environment shell." << endl << "[PHP] Apache link successful." << endl << endl;
				}
				
				else
				{
					phplink = false;
					cout << "[PHP] Unlinking PHP from Apache was successful." << endl << endl;
				}
			}
		}
		
		else if(cmd == "apache")
		{
			if(apache == false)
			{
				apache = true;
				cout << "[Apache] Starting Apache service..." << endl << endl;
			}
			
			else
			{
				apache = false;
				
				if(phplink == true)
				{
					phplink = false;
					cout << "[PHP] Unlinking PHP from Apache was successful." << endl; 
				}
				
				cout << "[Apache] Stopping Apache service..." << endl << endl;
			}
		}
		
		else if(cmd == "nginx")
		{
			cout << "[Nginx] Где?! Это Hostinger, забудь об этом." << endl << endl;
		}
		
		else if(cmd == "ftp")
		{
			if(ftpserver == false)
			{
				ftpserver = true;
				cout << "[Ftpd] Starting vsftpd server..." << endl << endl;
			}
			
			else
			{
				ftpserver = false;
				cout << "[Ftpd] Stopping vsftpd server..." << endl << endl;
			}
		}
		
		else if (cmd == "showcode")
		{
			if(lastcode == 0)
			{
				cout << "[Showcode] Вы ещё ничего не написали." << endl;
			}
			
			else if(lastcode == 1)
			{
				cout << "#include <iostream>" << endl <<
				"using namespace std;" << endl << endl <<
				"int main() {" << endl <<
				"for(int i = 0; ; i++) {" << endl <<
				"double *mas[i];" << endl <<
				"double *nas[i];" << endl <<
				"}" << endl << endl;
			}
			
			else if(lastcode == 2)
			{
				// Скоро будет
			}
			
			else if(lastcode == 3)
			{
				// Скоро будет
			}
			
			else if(lastcode == 4)
			{
				// Скоро будет
			}
			
			else if(lastcode == 5)
			{
				// Скоро будет
			}
			
			else if(lastcode == 6)
			{
				// Скоро будет
			}
		}
		
		else if(cmd == "c++")
		{
			lastcode = 1;
			cout << "[Visual Pudio 2014] Вы успешно написали на программу на C++." << endl <<
			"[Visual Pudio 2014] Посмотрите на неё, используя showcode." << endl;
		}
		
		else if(cmd == "c#")
		{
			lastcode = 2;
			cout << "[Visual Shpudio 2014] Вы попытались написать программу на C#." << endl <<
			"[Visual Shpudio 2014] Посмотрите на говнокод, используя showcode." << endl;
		}
		
		else if(cmd == "java")
		{
			lastcode = 3;
			cout << "[Eclipse] Вы написали мод для Minecraft." << endl <<
			"[Eclipse] Посмотрите на мод, используя showcode." << endl;
		}
		
		else if(cmd == "pascal")
		{
			lastcode = 4;
			cout << "[Turbo Pascal] Вы написали Hello World на Pascal." << endl <<
			"[Turbo Pascal] Посмотрите на Hello World, используя showcode." << endl;
		}
		
		else if(cmd == "assambler")
		{
			lastcode = 5;
			cout << "[Assambler] Вы покопались в исходнике калькулятора, но" << endl <<
			"не поняли, за что отвечают цифры." << endl <<
			"[Assambler] Посмотрите на исходник калькулятора, используя showcode." << endl;
		}
		
		else if(cmd == "pavlocode")
		{
			lastcode = 6;
			cout << "[Notepad++] Вы попытались написать программу для ПК на PHP." << endl <<
			"[Notepad++] Посмотрите на программу, используя showcode." << endl;
		}
		
		else
		{
			cout << "\"" << cmd << "\"" << " не является внутренней или внешней" << endl <<
			"командой, исполняемой программой или пактным файлом." << endl << endl;
		}
	}
	cin.clear();
	cin.sync();
	return 0;
}